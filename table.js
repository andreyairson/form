// * Get Local
const clear = document.getElementById("clear");

const handleFetchAPI = async (address) => {
  try {
    console.log(`Request started`);
    const response = await fetch(
      `https://nominatim.openstreetmap.org/search?q=${address}&format=json`
    ).then((data) => data.json());
    const { lat, lon } = response[0];
    return {
      res: { lat, lon },
      success: true,
    };
  } catch (err) {
    console.error(`Something was wrong...: ${err}`);
    return {
      success: false,
    };
  } finally {
    console.log(`Request finish`);
  }
};

const setData = () => {
  const dataLocal = Object.values(localStorage);
  const jsonData = JSON.parse(dataLocal);

  jsonData.forEach(async (element) => {
    const user = { id: Math.random() };
    const dl = document.createElement("dl");
    const dt = document.createElement("dt");
    const buttonCollapseRow = document.createElement("section");
    const nameUser = document.createElement("p");
    const phoneUser = document.createElement("a");
    const addressNotes = document.createElement("div");
    const address = document.createElement("div");
    const addressDd = document.createElement("dd");
    const mapa = document.createElement("div");
    const notes = document.createElement("div");
    const titleNotes = document.createElement("dd");
    const notesDd = document.createElement("dd");
    const notesDiv = document.createElement("div");
    const imgAsp = document.createElement("img");
    const pNotes = document.createElement("p");
    const headerCollapse = document.querySelector(".collapse-rows");

    dl.classList.add("content-json");
    buttonCollapseRow.setAttribute("aria-controls", user.id);
    buttonCollapseRow.setAttribute("aria-expanded", "false");
    addressNotes.classList.add("addressNotes");
    addressNotes.setAttribute("id", user.id);
    address.classList.add("address");
    addressDd.append("Address: ");
    nameUser.append(element.name);
    phoneUser.append(element.phone);
    phoneUser.setAttribute("href", `tel:${element.phone}`);
    mapa.setAttribute("aria-address", element.address);
    mapa.setAttribute("id", "map");
    notes.classList.add("notes");
    titleNotes.append("Notes: ");
    imgAsp.setAttribute("src", "./assets/format_quote_black_24dp.svg");
    pNotes.append(element.notes);
    dl.appendChild(dt);
    dt.appendChild(buttonCollapseRow);
    buttonCollapseRow.appendChild(nameUser);
    buttonCollapseRow.appendChild(phoneUser);
    dl.appendChild(addressNotes);
    addressNotes.appendChild(address);
    addressNotes.appendChild(notes);
    address.appendChild(addressDd);
    address.appendChild(mapa);
    notes.appendChild(titleNotes);
    notes.appendChild(notesDd);
    notesDd.appendChild(notesDiv);
    notesDiv.appendChild(imgAsp);
    notesDiv.appendChild(pNotes);
    headerCollapse.appendChild(dl);
  });

  const btndata = document.querySelectorAll(".content-json dt section");
  const activedData = (e) => {
    const section = e.currentTarget;
    const currAriaControl = e.currentTarget.getAttribute("aria-controls");
    const divsDetails = document.querySelectorAll(".addressNotes");

    btndata.forEach((btn) => {
      const btnAriaControl = btn.getAttribute("aria-controls");
      if (btnAriaControl === currAriaControl) {
        divsDetails.forEach((div) => {
          const divDetail = document
            .getElementById(btnAriaControl)
            .getAttribute("id");

          const currDiv = div.getAttribute("id");

          if (divDetail === currDiv) {
            div.classList.toggle("actived");
            const yes = div.classList.contains("actived");
            section.setAttribute("aria-expanded", yes);

            return;
          }
          return div.classList.remove("actived");
        });

        return;
      }

      return;
    });
  };

  const handleClear = () => {
    localStorage.clear();
    location.reload();
  };

  btndata.forEach((data) => data.addEventListener("click", activedData));
  clear.addEventListener("click", handleClear);
};

function initMap() {
  const maps = document.querySelectorAll("#map");

  maps.forEach(async (mapa) => {
    // console.log(mapa)
    const address = mapa.getAttribute("aria-address");
    const {
      res: { lat, lon },
    } = await handleFetchAPI(address);

    const coordenada = {
      lat: Number(lat),
      lng: Number(lon),
    };
    const map = new google.maps.Map(mapa, {
      zoom: 15,
      center: coordenada,
    });
    const marker = new google.maps.Marker({
      position: coordenada,
      map: map,
    });
  });
}

setData();
