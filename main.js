// * Form
const form = document.getElementById("forms");
const inpN = document.getElementById("name");
const inpP = document.getElementById("phone");
const inpA = document.getElementById("address");
const inpNt = document.getElementById("notes");
const submit = document.getElementById("submit");
const reset = document.getElementById("reset");
const formData = {
  name: "",
  phone: "",
  address: "",
  notes: "",
};
const divName = document.querySelector(".inptName");
const divPhone = document.querySelector(".inptPhone");
const divAddress = document.querySelector(".inptAddress");
const divNotes = document.querySelector(".area");

const handleChange = ({ target }) => {
  console.log(target);
  if (!target.checkValidity()) {
    if (form.name === target) {
      target.nextElementSibling.innerText =
        "This field needs at least 3 characters!";
      target.classList.add("invalid");
    }
    if (form.phone === target) {
      target.nextElementSibling.innerText =
        "This field needs at least 8 characters!";
      target.classList.add("invalid");
    }
    if (form.address === target) {
      target.nextElementSibling.innerText =
        "Fill in this field with your street, neighborhood and city!";
      target.classList.add("invalid");
    }
    if (form.notes === target) {
      target.nextElementSibling.innerText =
        "This field needs at least 10 characters!";
      target.classList.add("invalid");
    }
  } else {
    target.classList.remove("invalid");
    target.nextElementSibling.innerText = "";
  }

  const name = target.name;
  const value = target.value;
  formData[name] = value;
};

const arrayUser = localStorage.getItem("users")
  ? [...JSON.parse(localStorage.getItem("users"))]
  : [];

const saveData = (name, value) => {
  localStorage[name] = value;
};
const handleSubmit = () => {
  if (
    !inpN.checkValidity() ||
    !inpP.checkValidity() ||
    !inpA.checkValidity() ||
    !inpNt.checkValidity()
  ) {
    return;
  }

  arrayUser.push(formData);
  saveData("users", JSON.stringify(arrayUser));
  window.location.href = "./table.html";
};

const handleReset = () => {
  inpN.value = "";
  inpP.value = "";
  inpA.value = "";
  inpNt.value = "";
};

form.addEventListener("change", handleChange);
submit.addEventListener("click", handleSubmit);
reset.addEventListener("click", handleReset);
