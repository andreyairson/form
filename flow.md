# FLOW DYNAMIC TABLE EXERCISE

- Criar arquivos
  - index.html
  - style.css
  - main.js
  - flow.md
- table.html
- table.css
- data.json
- folder com nome assets
  - svg da seta
  - svg da aspas real

- Fazer o Json e todo o html + css das duas páginas (a tabela será populada inicialmente com dados aleatórios e fixos)
  Form

  - adicionar os ids nos inputs e textareas
  - lembrar de deixar o código o mais limpo e intuitivo possível para leitura

- O botão do reset ao clicado tem que colocar os valores default

  - quando clicado ele terá que retornar os valores default de todas os inputs(de forma nativa)

- criar a função que lida com o click do botão submit(pois quando ele for clicado ele irá validar os dados do form e redirecionar para a page da tabela e salvar os dados como um objeto dentro da sua array do json)
  - captura todos os valores dos inputs
    - depois verifica se nenhum é vazio
      - caso tenha algum vazio
        - se algum campo estiver vazio o botão de submit será desativado até que todos os inputs estejam válidos novamente.
    - Se tudo estiver preenchido
      - Inicie a parte da função que dá o post no seu json.
      - depois de salvar, o usuário será redirecionado para a página que contém a tabela

# Tabela

- Na página da tabela de o fetch no seu json (antes disso a tabela precisa estar pronta com dados hardcoded(dados falsos). Com o collapse/expand em cada linha funcionando, aí você se preocupa em integrar)

  - use um loop para conseguir ler tudo que capturou neste json, pois esses dados serão necessários para popular cada uma das linhas da tabela e suas respectivas collapsible rows
    - monte esse loop com cada uma das rows
      - assim que os dados reais estiverem sendo mostrados em tela

- Com os dados que você recebeu do fetch você terá que fazer as seguintes ações

  - Você irá colocar os dados do nome e telefone que recebeu do seu fetch, nas respectivas células da linha da tabela, e logo ao lado, irá renderizar o botão de abrir e fechar a linha de baixo

    - e imprimir esses dados no seu html da row
    - o botão de ação de cada linha terá que ter uma seta que gira 180°
      - quando clicado a seta fica pra baixo
    - pense na funcionalidade do botão de abrir e fechar
      - sendo que só pode ter uma row aberta por vez

  - terá que fazer uma função para os dados que você receber do address
    - essa função recebe como parâmetro os dados de endereço de cada usuário contido no array de objetos que obtivemos pelo fetch
      - e dentro da função ela transformará ele em um google maps embed
        - google maps embed será colocado dentro do seu collapse row
          - abaixo do "ADDRESS"
  - Ao dar o fetch você receberá os dados do Notes
    - depois de ter os dados, irá imprimi-lo no seu collapse row
      - abaixo da parte "NOTES"

- Obs.: uma dica: você pode, na linha de baixo, atribuir o id do usuário selecionado, que vai ser o mesmo id da table row, para lidar com o abrir e fechar.
  esse id será gerado com a função Math.random(), que cria um id único quando usada. deverá ser adicionado no objeto do usuário logo antes de cadastrá-lo no arquivo JSON

# INFORMAÇÕES ADICIONAIS :

- Tech Stack
- JSON files management
- JS loops
- HTML5 Tables
- Arrow Functions
- JS Fetch
- 3rd party plugins
- google maps embeded
- DOM events
- CSS pseudo-classes and transitions

# REFERENCIAS DADAS PARA O PROJETO :

- maps:
  https://blog.duda.co/pt-br/adicionar-google-maps-no-site
  https://developers.google.com/maps/documentation/javascript/adding-a-google-map#maps_add_map-html

- formulario:
  https://www.origamid.com/slide/javascript-completo-es6/#/0512-forms/7

- collaps rows:
  https://www.creativebloq.com/javascript/how-create-collapsible-css-tables-10097141
  https://www.origamid.com/curso/javascript-completo-es6/0515-localstorage

- zebra:
  https://i.stack.imgur.com/G6xXh.
  https://www.origamid.com/slide/html-e-css-para-iniciantes/#/0504-pseudo-classes/3


<!-- # FIRST STEP

- Criar o HTML Index

  - 1 form para englobar
  - 1 h1
  - 4 divs {1 para cada input}
  - 4 inputs
    - Name
    - Phone
    - Adress
    - Notes
  - 2 buttons
    - Submit
    - Reset

- Criar o HTML segunda tela
  - Criar 1 div para englobar
    - Criar 1 div interna para passar valores de referencia
      - Name
      - Phone
      - See More
    - Criar 1 div para receber os valores do form anterior
      - Dentro desta div tera um dl,dt e dd
        - O dt terá infromações de referencia que receberá do formulario que será
          - Name
          - Phone
          - Uma imagem de botão
        - Terá 4 dd
          - O primeiro receberá o "Adrees"
          - O segundo o mapa que virá de acordo com o adrees do form
          - O terceiro o "Notes"
          - O quarto receberá o conteudo dos notes

# SECOND STEP

- Criar CSS index

  - Background do form
  - Centralizar o h1
  - Delimitar um tamanho para input
    - Adicionar um display flex
    - Mudar a borda
    - Adicionar uma sombra
    - Mudar o Background

- Criar CSS segunda tela

  - Centralizar o h1
  - Fazer o estilo do Name, Phone, See More
  - Criar um dt com display grid
    - Um grid template collums auto 1fr auto
  - Criar um bloco especifico do dd dos conteudos do deste bloco
    - Ajeitar o tamanho de cada conteudo
      - fazer os dd com display none
      - fazer uma classe para mudar o display none do dd para display block.

# THIRD STEP

    QUANDO EU CLICAR NO SUBMIT, TODOS OS DADOS SERÃO ADICIONADOS EM UM ARQUIVO JSON E A PAGINA SERÁ REDIRECIONADO PARA A SEGUNDA PAGINA.
    O DT IRÁ RECEBER O NAME E NUMBER(ESTARÁ DENTRO DO JSON)
    O DD QUE ESTERÁ EMBAIXO DO ADRESS TERÁ UM LINK DO GOOGLE QUE SERÁ EXATAMENTE O QUE TEM DENTRO DO SEU ADRESS QUE ESTA NO JASON.
    O DD QUE ESTARÁ EMBAIXO DO NOTES, TERÁ TODOS OS DADOS DO NOTE(QUE ESTARÁ NO JSON)

    zebra(https://i.stack.imgur.com/G6xXh.png)

<!-- criar um json para salvar os dados
colaps table
tablea striped
se for odd mude de cor.
quando sair da page o json será terá abastecido com as informações

e2e visão do formulario -->

<!-- # FLOW

- Page index

  - 1 h1 centralizado
  - Input
    - estabelecer id unico para cada
  - submit
    - ir para outra pagina
      - e quando clicado adicionar todos os dados dos inputs para o arquivo json
      - uma verificação que só poderá ir para outra pagina se todos os campos estiverem preenchidos
  - reset - resetar os dados do input para default

- User List
  - 1 h1 centralizado
  - Uma div com referencia de nome phone e see more
  - 1 dl para os dados que serão recebidos
    - 1 dt que terá um button
      - este button receberá o nome telefone (que serão importados do json) e uma imagem
        - a imagem irá girar em 180° quando for clicada
    - 4 dl
      - 1 para referencia de address
      - 1 terá a imagem do google maps referente ao endereço que estará no json
      - 1 terá referencia de notes
      - 1 terá o conteudo de notes que está dentro do json -->

<!-- - Fazer todo o html + css e o Json das duas páginas (a tabela será com dados aleatórios e fixos)

  - adicionar os ids nos inputs e textareas
  - lembrar de deixar o código o mais acessível possível para leitura

- criar uma função para o button reset

  - quando clicado ele terá que retornar os valores default de todas os inputs

- criar a função que lida com o click do botão submit(pois quando ele for clicado ele irá criar toda a collapse row da segunda pagina)
  - captura todos os valores dos inputs
    - depois verifica se nenhum é vazio
      - caso tenha algum vazio
        - acione a função que irá bloquear a troca de página até todos que todos os dados sejam preenchidos
    - Se tudo estiver preenchido
      - inicie a parte da função que dá o post no seu json
  - Na página da tabela de o fetch no seu json
    - use um loop para conseguir ler tudo que capturou neste json e enviar para a segunda page pois esses dados serão necessários para o collapse row
      - monte esse loop com cada uma das rows, ignorando a parte que abre e fecha
        - assim que os dados reais estiverem sendo mostrados em tela

- Com os dados que você recebeu do no seu json você terá que fazer as seguintes ações
  - Função do collapse row
    - terá que receber o nome e telefone que você recebeu do formulário
      - e imprimir esses dados no seu html da row
      - e tambem terá que ter uma seta que gira 180°
        - quando clicado a seta fica pra baixo
    - pense na funcionalidade do botão de abrir e fechar
      - sendo que só pode ter uma row aberta por vez
  - terá que fazer uma função para os dados que você receber do address
    - ela terá que receber os dados já preenchidos
      - e dentro da função ela transformará ele em uma imagem do maps
        - esta imagem será colocado dentro do seu collapse row
          - abaixo do "ADDRESS"
  - terá que fazer uma função para os dados que você receber do notes
    - quer irá receber os dados do json
    - depois de ter os dados, irá imprimi-lo no seu collapse row
      - abaixo da parte "NOTES"

Obs.: uma dica: você pode, na linha de baixo, atribuir um id, que vai ser o mesmo id da table row, para lidar com o abrir e fechar -->
